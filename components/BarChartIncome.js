import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'

import moment from 'moment'

export default function BarChartIncome({rawData}) {

    //console.log(rawData)

    const [months, setMonths] = useState([])
    const [monthlyIncome, setMonthlyIncome] = useState([])

    
    useEffect(() => {
        
        setMonths(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])

    }, [rawData])

    useEffect(() => {
        
        setMonthlyIncome(months.map(month => {
            let sales = 0 
            rawData.forEach(element => {
                if (moment(element.date).format("MMMM") === month) {
                    sales += parseInt(element.amount)
                }
            })
            return sales
        }))
    }, [months])
    
    const data = {
        labels: months, 
        datasets: [{
            label: 'Income', 
            backgroundColor: 'blue',
            borderColor: 'white',
            borderWidth: 1,
            hoverBackgroundColor: 'lightblue',
            hoverBorderColor: 'black',
            data: monthlyIncome 
        }]
    }

    const options = {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,                   
                    }
                }
            ]
        }
    }

    return (
        <Bar data={data} options={options} />
    )

}
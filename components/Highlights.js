import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights() {

	return (

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Money Management</h2>
						</Card.Title>
						<Card.Text>
							Money management is a strategic technique to make money yield the highest interest-output value for any amount spent. The idea of money management techniques has been developed to reduce the amount that individuals, firms, and institutions spend on items that add no significant value to their living standards, long-term portfolios, and assets.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Why is Budgeting so Important?</h2>
						</Card.Title>
						<Card.Text>
							Since budgeting allows you to create a spending plan for your money, it ensures that you will always have enough money for the things you need and the things that are important to you.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Importance of Saving Money</h2>
						</Card.Title>
						<Card.Text>
							It allows you to enjoy greater security in your life. If you have cash set aside for emergencies, you have a fallback should something unexpected happen. And, if you have savings set aside for discretionary expenses, you may be able to take risks or try new things.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}
import { useContext } from 'react'

import { Navbar, Nav } from 'react-bootstrap'

import Link from 'next/link'

import UserContext from '../userContext'

import Head from 'next/head'

export default function NavBar() {

	const { user } = useContext(UserContext)

	// console.log(user)

	return (
		<>
			<Head>
				<title>
					Budget Tracker
        		</title>
			</Head>
			<Navbar bg="light" expand="lg">
				<Link href="/">
					<a className="navbar-brand">JET BT</a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<Link href="/about">
							<a className="nav-link" role="button">About</a>
						</Link>
					</Nav>
					<Nav className="ml-auto">
						<Link href="/">
							<a className="nav-link" role="button">Home</a>
						</Link>
						{
							user.email
								?
								<>
									<Link href="/profile">
										<a className="nav-link" role="button">Profile</a>
									</Link>
									<Link href="/categories">
										<a className="nav-link" role="button">Categories</a>
									</Link>
									<Link href="/categories/transactions">
										<a className="nav-link" role="button">Transactions</a>
									</Link>
									<Link href="/categories/incomeStatistics">
										<a className="nav-link" role="button">Income Statistics</a>
									</Link>
									<Link href="/categories/expenseStatistics">
										<a className="nav-link" role="button">Expense Statistics</a>
									</Link>
									<Link href="/categories/allStatistics">
										<a className="nav-link" role="button">All Statistics</a>
									</Link>
									<Link href="/logout">
										<a className="nav-link" role="button">Logout</a>
									</Link>
								</>
								:
								<>
									<Link href="/register">
										<a className="nav-link" role="button">Register</a>
									</Link>
									<Link href="/login">
										<a className="nav-link" role="button">Login</a>
									</Link>
								</>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		</>
	)
}
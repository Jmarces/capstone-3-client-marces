import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'

import randomcolor from 'randomcolor'

export default function PieChart({rawData}) {

    //console.log(rawData)
    
    const [category, setCategory] = useState([])
    const [amount, setAmount] = useState([])
    const [randomBGColors, setRandomBGColors] = useState([])

    useEffect(() => {
        // segregate the brands and sales into their respective array
        setCategory(rawData.map(element => element.category))
        setAmount(rawData.map(element => element.amount))
        // for each item in the array, we will return a random hex decimal as colors
        setRandomBGColors(rawData.map(() => randomcolor()))
    }, [rawData])

    const data = {
       labels: category,
       datasets: [{
           data: amount,
           backgroundColor: randomBGColors
       }]
    }
    return (
        <Pie data={data} />
    )
}
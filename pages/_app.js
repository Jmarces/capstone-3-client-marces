import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'

import { useState, useEffect } from 'react'

import { Container } from 'react-bootstrap'

import NavBar from '../components/NavBar'

import { UserProvider } from '../userContext'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null
	})

	useEffect(() => {
		setUser({
			email: localStorage.getItem('email')
		})
	}, [])

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			email: null
		})
	}

	return (

		<>
			<UserProvider value={{ user, setUser, unsetUser }}>
				<NavBar />
				<Container>
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</>
	)
}

export default MyApp
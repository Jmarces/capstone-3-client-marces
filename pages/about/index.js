import Banner from '../../components/Banner'
import Highlights from '../../components/Highlights'

import Image from 'next/image'

import { Carousel } from 'react-bootstrap'

export default function About() {

	const data = {

		title: "About Jets Budget Tracker!",
		content: "Tracking Budgets since 2021",
		destination: '/categories'
	}

	return (

		<>
			<Banner dataProp={data} />

			<Carousel className="my-5">
				<Carousel.Item interval={50000}>
					<Image
						src="/images/Budget.png"
						alt="Picture of the author"
						width={1200}
						height={500}
					/>
					<Carousel.Caption>
						<h3 className="bg-secondary">Budget Tracker App</h3>
						<p className="bg-secondary">This Budget Tracker App was made using MERN Stack with Next JS. Although not perfect, the coding was the result of an epic struggle during Zuitt Bootcamp Batch 92. Please do check all the features and appreciate its simplicity.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item interval={50000}>
					<Image
						src="/images/Coding.png"
						alt="Picture of the author"
						width={1200}
						height={500}
					/>
					<Carousel.Caption>
						<h3 className="bg-secondary">The Inception of an Aspiring Web Developer</h3>
						<p className="bg-secondary">My name is Jet Marces. Aspiring Programmer and career-shifter. Making this App as a final project during bootcamp was extremely progressive. Huge hurdles were made but it doesn't mean it's over. We keep moving forward and go beyond limits.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item interval={50000}>
					<Image
						src="/images/Apo.jpg"
						alt="Picture of the author"
						width={1200}
						height={500}
					/>
					<Carousel.Caption>
						<h3 className="bg-secondary">Coding and Nature</h3>
						<p className="bg-secondary">While starting the creation of this App, all I could think of was coding and sitting infront of a computer. However, I always took time for breaks, take a walk outside or look out the window watching the skies and the mountains far out. Although I am in the path of overseeing computers and algorithms, nothing compares to the fascination of nature and how it works in a way similar to coding.</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
		</>

	)
}
import { useState, useEffect, useContext } from 'react'

import PieChart from '../../components/PieChart'

export default function AllStatistics() {

    const [transactions, setTransactions] = useState([])

    useEffect(() => {

        let token = localStorage.getItem('token')

        fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions', {
            method: 'GET',
            headers: {
                'Authorization': `bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setTransactions(data)
            })
    }, [])

    let holder = {};

    transactions.forEach(function (i) {
        if (holder.hasOwnProperty(i.category)) {
            holder[i.category] = holder[i.category] + i.amount;
        } else {
            holder[i.category] = i.amount;
        }
    });

    let categoryTotals = [];

    for (let prop in holder) {
        categoryTotals.push({ category: prop, amount: holder[prop] });
    }

    return (
        <>  
            <h1 className="text-center">All Statistics Chart</h1>
            <PieChart rawData={categoryTotals} />
        </>
    )
}
import { useState, useEffect, useContext } from 'react'

import { Row, Table, Button } from 'react-bootstrap'

import UserContext from '../../userContext'

import BarChartExpense from '../../components/BarChartExpense'

export default function expenseStatistics() {

	const [allExpense, setAllExpense] = useState([])

	useEffect(() => {

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions/expense', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setAllExpense(data)
			})
	}, [])

	return (
		<>
			<BarChartExpense rawData={allExpense} />
		</>
	)
}
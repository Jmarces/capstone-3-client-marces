import { useState, useEffect, useContext } from 'react'

import { Row, Table, Button } from 'react-bootstrap'

import UserContext from '../../userContext'

import BarChartIncome from '../../components/BarChartIncome'

export default function incomeStatistics() {

	const [allIncome, setAllIncome] = useState([])

	useEffect(() => {

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions/income', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setAllIncome(data)
			})
	}, [])

	return (
		<>
			<BarChartIncome rawData={allIncome} />
		</>
	)
}
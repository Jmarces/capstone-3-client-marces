import { useState, useEffect, useContext } from 'react'

import { Form, Button, Row, Table, Card } from 'react-bootstrap'

import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../../userContext'

export default function Categories() {

	const { user } = useContext(UserContext)

	// console.log(user)

	const [name, setName] = useState("")
	const [type, setType] = useState("")

	const [categories, setCategories] = useState([])

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/categories', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setCategories(data)
			})

		if (name !== "" && type !== "") {

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [name, type])

	function addCategory(e) {

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/transactions/category', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `bearer ${token}`
			},
			body: JSON.stringify({
				category: name,
				type: type
			})
		})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					Swal.fire({

						icon: "success",
						title: "Category Added.",
						text: `"${name}" has been added as a category for ${type}.`
					})

					fetch('https://murmuring-stream-31656.herokuapp.com/api/users/categories', {
						method: 'GET',
						headers: {
							'Authorization': `bearer ${token}`
						}
					})
						.then(res => res.json())
						.then(data => {
							setCategories(data)
						})

				} else {

					Swal.fire({

						icon: "error",
						title: "Add Category Failed",
						text: "Something went wrong."
					})
				}
			})

		setName("")
		setType("")
	}
	
	let categoriesRows = categories.map(i => {
		return (

			<tr key={i._id}>
				<td>{i.category}</td>
				<td>{i.type}</td>
			</tr>
		)
	})

	let data = {

		title: "Error 403",
		content: "You don't have enough permission to view this page.",
		destination: "/",
		label: "Back to the Home Page"
	}

	return (
		<>
			<Row>
				<div className="col-12 col-md-6">
					<h1 className=" my-4 text-center">Add Category</h1>
					<Form onSubmit={e => addCategory(e)}>
						<Form.Group controlId="name">
							<Form.Label>Category Name</Form.Label>
							<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="categoryType">
							<Form.Label>Category Type</Form.Label>
							<Form.Control as="select" type="text" value={type} onChange={e => setType(e.target.value)} required >
								<option></option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
						{
							isActive
								?
								<Button type="submit" variant="primary">Add Category</Button>
								:
								<Button type="submit" variant="primary" disabled>Add Category</Button>
						}
					</Form>
				</div>
				<div className="col-12 col-md-6">
					<h1 className="my-4 text-center">Categories</h1>
					<Table striped bordered hover className="text-center" responsive="lg">
						<thead>
							<tr>
								<th>Name</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody >
							{categoriesRows}
						</tbody>
					</Table>
				</div>
			</Row>
		</>
	)
}
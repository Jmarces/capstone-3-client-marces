import { useState, useEffect, useContext } from 'react'

import { Form, Button, Row, Table, Card, InputGroup } from 'react-bootstrap'

import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../../userContext'

export default function Transactions() {

	const { user } = useContext(UserContext)

	// console.log(user)

	const [name, setName] = useState("")
	const [type, setType] = useState("")
	const [amount, setAmount] = useState(0)
	const [description, setDescription] = useState("")

	const [categories, setCategories] = useState([])
	const [transactions, setTransactions] = useState([])
	const [balance, setBalance] = useState(0)

	const [search, setSearch] = useState(" ")
	const [filter, setFilter] = useState("")

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setTransactions(data)
			})

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/categories', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setCategories(data)
			})

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions/amount', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setBalance(data)
			})



		if (name !== "" && type !== "" && amount !== 0 && description !== "") {

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [name, type, amount, description])

	function addTransaction(e) {

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://murmuring-stream-31656.herokuapp.com/api/transactions/category/transaction', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `bearer ${token}`
			},
			body: JSON.stringify({
				category: name,
				type: type,
				amount: amount,
				description: description
			})
		})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					Swal.fire({

						icon: "success",
						title: "Transaction Added.",
						text: `"${amount}" has been added as a transaction for ${name}(${type}).`
					})

					fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions', {
						method: 'GET',
						headers: {
							'Authorization': `bearer ${token}`
						}
					})
						.then(res => res.json())
						.then(data => {
							setTransactions(data)
						})

					fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions/amount', {
						method: 'GET',
						headers: {
							'Authorization': `bearer ${token}`
						}
					})
						.then(res => res.json())
						.then(data => {
							setBalance(data)
						})

				} else {

					Swal.fire({

						icon: "error",
						title: "Add Transaction Failed",
						text: "Something went wrong."
					})
				}
			})

		setName("")
		setType("")
		setAmount(0)
		setDescription("")
	}


	let transactionRows = transactions.map(i => {
		let date = i.date.slice(0, 10)

		if (i.type.match(filter)) {
			return (

				<tr key={i._id}>
					<td>{i.category}({i.type})</td>
					<td>{i.description}</td>
					<td>{i.amount}</td>
					<td>{date}</td>
				</tr>

			)
		} else if (filter === "All") {
			return (

				<tr key={i._id}>
					<td>{i.category}({i.type})</td>
					<td>{i.description}</td>
					<td>{i.amount}</td>
					<td>{date}</td>
				</tr>

			)
		}
	})

	let searchRows = transactions.map(i => {
		let date = i.date.slice(0, 10)

		if ((i.category.match(search)) || (i.description.match(search))) {

			return (

				<tr key={i._id}>
					<td>{i.category}({i.type})</td>
					<td>{i.description}</td>
					<td>{i.amount}</td>
					<td>{date}</td>
				</tr>

			)
		}
	})

	let categoriesName = categories.map(i => {
		// console.log(i)
		// console.log(type)
		if (i.type === type) {
			return (
				<>
					<option key={i._id}>{i.category}</option>
				</>
			)
		} else if (i.type === type) {
			return (
				<>
					<option key={i._id}>{i.category}</option>
				</>
			)
		}
	})

	let data = {

		title: "Error 403",
		content: "You don't have enough permission to view this page.",
		destination: "/",
		label: "Back to the Home Page"
	}

	return (
		<>
			<Row>
				<div className="col-12 col-md-6">
					<h1 className=" my-4 text-center">Add Transaction</h1>
					<Form onSubmit={e => addTransaction(e)}>
						<Form.Group controlId="categoryType">
							<Form.Label>Category Type</Form.Label>
							<Form.Control as="select" type="text" value={type} onChange={e => setType(e.target.value)} required >
								<option></option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="categoryName">
							<Form.Label>Category Name</Form.Label>
							<Form.Control as="select" type="text" value={name} onChange={e => setName(e.target.value)} required >
								<option></option>
								{categoriesName}
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="amount">
							<Form.Label>Amount</Form.Label>
							<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e => setAmount(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="description">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>
						{
							isActive
								?
								<Button type="submit" variant="primary">Add Transaction</Button>
								:
								<Button type="submit" variant="primary" disabled>Add Transaction</Button>
						}
					</Form>
					<h1 className=" my-4 text-center">Search Transaction</h1>
					<Form>
						<Form.Group controlId="search">
							<Form.Control type="text" placeholder="Search Category/Description" value={search} onChange={e => setSearch(e.target.value)} required />
						</Form.Group>
					</Form>
					<Table striped bordered hover className="text-center" responsive="lg">
						<thead>
							<tr>
								<th>Category</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody >
							{searchRows}
						</tbody>
					</Table>
				</div>
				<div className="col-12 col-md-6">
					<h1 className="my-4 text-center">Transactions</h1>
					<Card className="text-center">
						<Card.Header>Current Balance</Card.Header>
						<Card.Body>
							<Card.Title>&#8369;{balance}</Card.Title>
						</Card.Body>
					</Card>
					<Form>
						<Form.Group controlId="filter">
							<Form.Control as="select" type="text" value={filter} onChange={e => setFilter(e.target.value)} required >
								<option>All</option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
					</Form>
					<Table striped bordered hover className="text-center" responsive="lg">
						<thead>
							<tr>
								<th>Category</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody >
							{transactionRows}
						</tbody>
					</Table>
				</div>
			</Row>
		</>
	)
}
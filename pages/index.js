import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home() {

	const data = {

		title: "Jets Budget Tracker!",
		content: "Tracking Budgets since 2021",
		destination: '/categories'
	}

	return (

		<>
			<Banner dataProp={data} />
			<Highlights />
		</>

	)
}
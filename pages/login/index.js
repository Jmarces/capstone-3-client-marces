import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'

import Swal from 'sweetalert2'

// Redirect the User, use the Router component from NextJS
import Router from 'next/router'

import UserContext from '../../userContext'

// Import the Google login component from react google login
import { GoogleLogin } from 'react-google-login'

export default function Login() {

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {

		if (email !== "" && password !== "") {

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [email, password])

	function authenticate(e) {

		e.preventDefault()

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/login', {

			method: "POST",
			headers: {

				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})
		})
			.then(res => res.json())
			.then(data => {

				if (data.accessToken) {

					localStorage.setItem("token", data.accessToken)

					fetch('https://murmuring-stream-31656.herokuapp.com/api/users/details', {

						headers: {

							Authorization: `Bearer ${data.accessToken}`
						}

					})
						.then(res => res.json())
						.then(data => {

							localStorage.setItem("email", data.email)
				
							// after getting the user's details from the API server, we will set the global user state
							setUser({
								email: data.email					
							})
						})

					Swal.fire({

						icon: "success",
						title: "Successfully Logged In.",
						text: "Thank you for logging in."
					})

					Router.push('/categories')
				} else {

					Swal.fire({

						icon: "error",
						title: "Unsuccessful Login or Incorrect Login",
						text: "Please verify Email and/or Password."
					})
				}
			})

		setEmail("")
		setPassword("")
	}

	function authenticateGoogleToken(response) {

		// console.log(response)

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/verify-google-id-token', {

			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				/*
				For Email Sending:

					Pass the accessToken from google to allow us the use of Google API to send an email to the google login user who logs on for the first time.
				
				*/
				tokenId: response.tokenId,
				accessToken: response.accessToken
			})
		})
			.then(res => res.json())
			.then(data => {

				if (typeof data.accessToken !== 'undefined') {

					localStorage.setItem('token', data.accessToken)

					fetch('https://murmuring-stream-31656.herokuapp.com/api/users/details', {

						headers: {

							Authorization: `Bearer ${data.accessToken}`
						}

					})
						.then(res => res.json())
						.then(data => {

							localStorage.setItem("email", data.email)


							setUser({

								email: data.email

							})
						})

					Swal.fire({

						icon: "success",
						title: "Successfully Logged In.",
						text: "Thank you for logging in."
					})

					Router.push('/categories')
				} else {

					if (data.error === "google-auth-error") {

						Swal.fire({

							icon: "error",
							title: "Google Authentication Failed"
						})
					} else if (data.error === "login-type-error") {

						Swal.fire({

							icon: "error",
							title: "Login Failed",
							text: "You may have registered through a different procedure."
						})
					}
				}
			})
	}

	return (
		<>
		<h1 className="text-center">Log In</h1>
		<Form onSubmit={e => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="userPassword">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required />
			</Form.Group>
			{
				isActive
					?
					<Button variant="primary" type="submit" className="btn-block">Log In</Button>
					:
					<Button variant="primary" className="btn-block" disabled>Log In</Button>
			}
			<Button href="/register"variant="primary" type="submit" className="btn-block">Not Yet Registered? Sign Up!</Button>
			<GoogleLogin

				clientId="313789917102-m4ts5ek9ba0cm9buqgnu1ec2055h67nh.apps.googleusercontent.com"
				buttonText="Login using Google"
				onSuccess={authenticateGoogleToken}
				onFailure={authenticateGoogleToken}
				cookiePolicy={'single_host_origin'}
				className="w-100 text-center my-4 d-flex justify-content-center"

			/>
		</Form>
		</>
	)
}
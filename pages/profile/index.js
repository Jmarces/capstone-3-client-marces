import { useState, useEffect, useContext } from 'react'

import { Card, ListGroup } from 'react-bootstrap'

import UserContext from '../../userContext'

export default function Profile() {

    const [details, setDetails] = useState([])
    const [balance, setBalance] = useState([])

    useEffect(() => {

        let token = localStorage.getItem('token')

        fetch('https://murmuring-stream-31656.herokuapp.com/api/users/details', {
            method: 'GET',
            headers: {
                'Authorization': `bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setDetails(data)
            })

        fetch('https://murmuring-stream-31656.herokuapp.com/api/users/transactions/amount', {
            method: 'GET',
            headers: {
                'Authorization': `bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setBalance(data)
            })
    }, [])

    return (

        <div className="col-12 col-md-6">
            <h1>Profile Details</h1>
            <Card style={{ width: '18rem' }}>
                <ListGroup variant="flush">
                    <ListGroup.Item><strong>Name:</strong> {details.firstName} {details.lastName}</ListGroup.Item>
                    <ListGroup.Item><strong>Email:</strong> {details.email}</ListGroup.Item>
                    <ListGroup.Item><strong>Current Balance:</strong> &#8369;{balance}</ListGroup.Item>
                </ListGroup>
            </Card>
        </div>
    )
}
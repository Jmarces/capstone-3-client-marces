import {useEffect,useState,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'

import Router from 'next/router'

import Swal from 'sweetalert2'

export default function Register(){

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState(0)
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	const [isActive,setIsActive] = useState(true)

	useEffect(() =>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11 )){

			setIsActive(true)
		}else {

			setIsActive(false)
		}
	},[firstName, lastName, email, mobileNo, password1, password2])

	function registerUser(e){

		e.preventDefault()

		fetch('https://murmuring-stream-31656.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === false) {

				fetch('https://murmuring-stream-31656.herokuapp.com/api/users', {
					method: 'POST',
					headers: {'Content-Type' : 'application/json'},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true){

						Swal.fire({

							icon: "success",
							title: "Successfully Registered.",
							text: `Thank you for registering to Jets Budget Tracker! "An investment in knowledge pays the best interest." --Benjamin Franklin`
						})

						Router.push('/login')
					}else {

						Swal.fire({

							icon: "error",
							title: "Registration Failed",
							text: "Something went wrong."
						})
					}
				})		
			}else {

				Swal.fire({
					icon: "error",
					title: "Registration Failed",
					text: "The Email Address has already been registered."
				})
			}
		})
	}

	return (
		<>
			<h1 className="text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e)=>setFirstName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e)=>setLastName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control type="text" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userMobile">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile Number" value={mobileNo} onChange={(e)=>setMobileNo(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userPassword1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={(e)=>setPassword1(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userPassword2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password2} onChange={(e)=>setPassword2(e.target.value)} required/>
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit">Register</Button>
					:<Button variant="primary" disabled>Register</Button>
				}
			</Form>	
		</>
	)
}